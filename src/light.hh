#ifndef __LIGHT__HH__
#define __LIGHT__HH__

#include "object.hh"
#include "collision.hh"
#include "ray.hh"
#include "util.hh"
#include "bmpWriter.hh"

class Light : public Object {
private:

public:

  Light();
  ~Light();

  Collision* collideWith(Ray& ray);

  void getPosFrom(float* position, float* newPos);
};

#endif
