#include "bmpWriter.hh"

BMPImage::BMPImage(int w, int h) {
  width = w;
  height = h;

  pixels = (uint32_t*) calloc(w*h , sizeof(uint32_t));
}

uint32_t BMPImage::getColourFromRGB(uint8_t r, uint8_t g, uint8_t b) {
  return (r << 16) + (g << 8) + b;
}

BMPImage::~BMPImage() {
  free(pixels);
}

void BMPImage::writeToFile(std::string filename) {
  int dataSize = width*height*sizeof(uint32_t);
  int fileSize = 54 + dataSize;

  unsigned char header[54] = {
    'B', 'M', // Magic word
    (unsigned char)fileSize, (unsigned char)(fileSize >> 8), (unsigned char)(fileSize >> 16), (unsigned char)(fileSize >> 24), // file size in bytes
    0, 0, 0, 0, // Reserved
    54, 0, 0, 0, // Offset to start of the image


    40, 0, 0, 0, // header size in bytes
    (unsigned char)width, (unsigned char)(width >> 8), (unsigned char)(width >> 16), (unsigned char)(width >> 24), // width in pixels
    (unsigned char)height, (unsigned char)(height >> 8), (unsigned char)(height >> 16), (unsigned char)(height >> 24), // height in pixels
    1, 0, // Number of image planes
    24, 0, // Bits per pixel (1, 4, 8, 24)
    0, 0, 0, 0, // Compression type
    (unsigned char)dataSize, (unsigned char)(dataSize >> 8), (unsigned char)(dataSize >> 16), (unsigned char)(dataSize >> 24), // Data size
    0x13, 0x0B, 0, 0, // Horizontal resolution (in pixels/meter)
    0x13, 0x0B, 0, 0, // Vertical resolution (in pixels/meter)
    0, 0, 0, 0, // Number of colours used
    0, 0, 0, 0 // Number of "important colours"
  };

  std::ofstream file;
  file.open(filename.c_str());

  for (int i = 0; i < 54; ++i) {
    file << header[i];
  }

  for (int i = 0; i < height; ++i) {
    for (int j = 0; j< width; ++j) {
      file << (unsigned char) pixels[j + i*width];
      file << (unsigned char) (pixels[j + i*width] >> 8);
      file << (unsigned char) (pixels[j + i*width] >> 16);
    }
  }

  file.close();
}

void BMPImage::writeColour(int i, int j, uint32_t colour) {
  pixels[j + i*width] = colour;
}

uint32_t* BMPImage::switchBuffer(bool realloc) {
  uint32_t* buffer = pixels;

  if (realloc) {
    pixels = (uint32_t*) calloc(width*height , sizeof(uint32_t));
  }

  return buffer;
}

void BMPImage::setBuffer(uint32_t* buffer) {
  if (pixels) {
    free(pixels);
  }

  pixels = buffer;
}


uint32_t BMPImage::addColours(uint32_t c1, uint32_t c2) {
  uint8_t r1 = (c1 & (255 << 16)) >> 16;
  uint8_t g1 = (c1 & (255 << 8)) >> 8;
  uint8_t b1 = (c1 & 255);

  uint8_t r2 = (c2 & (255 << 16)) >> 16;
  uint8_t g2 = (c2 & (255 << 8)) >> 8;
  uint8_t b2 = (c2 & 255);

  return BMPImage::getColourFromRGB(std::min(r1+r2, 255),
			  std::min(g1+g2, 255),
			  std::min(b1+b2, 255));
}

uint32_t BMPImage::multiplyColours(uint32_t c1, uint32_t c2) {
  uint8_t r1 = (c1 & (255 << 16)) >> 16;
  uint8_t g1 = (c1 & (255 << 8)) >> 8;
  uint8_t b1 = (c1 & 255);

  uint8_t r2 = (c2 & (255 << 16)) >> 16;
  uint8_t g2 = (c2 & (255 << 8)) >> 8;
  uint8_t b2 = (c2 & 255);

  uint8_t mR = (r1*r2)/255;
  uint8_t mG = (g1*g2)/255;
  uint8_t mB = (b1*b2)/255;

  return BMPImage::getColourFromRGB(mR, mG, mB);
}

void BMPImage::getRGBFromColour(uint32_t colour, uint8_t* rgb) {
  rgb[0] = (colour & (255 << 16))>>16;
  rgb[1] = (colour & (255 << 8))>>8;
  rgb[2] = colour & (255);
}

uint32_t BMPImage::getColourFromRGB(uint8_t* rgb) {
  return getColourFromRGB(rgb[0], rgb[1], rgb[2]);
}

uint32_t BMPImage::addProportionalColours(uint32_t c1, uint32_t c2, float perc) {
  uint8_t r1 = (c1 & (255 << 16)) >> 16;
  uint8_t g1 = (c1 & (255 << 8)) >> 8;
  uint8_t b1 = (c1 & 255);

  uint8_t r2 = (c2 & (255 << 16)) >> 16;
  uint8_t g2 = (c2 & (255 << 8)) >> 8;
  uint8_t b2 = (c2 & 255);

  uint8_t pR = (uint8_t) ( (1-perc)*r1 + perc*r2 );
  uint8_t pG = (uint8_t) ( (1-perc)*g1 + perc*g2 );
  uint8_t pB = (uint8_t) ( (1-perc)*b1 + perc*b2 );

  return BMPImage::getColourFromRGB(std::min(pR, (uint8_t) 255),
				    std::min(pG, (uint8_t) 255),
				    std::min(pB, (uint8_t) 255));
}

uint32_t BMPImage::meanColour(uint32_t* colours, int size) {
  uint32_t r = 0,
	   g = 0,
	   b = 0;
	   
  uint8_t rgb[3] = {};
  
  for(int i = 0; i < size; ++i) {
	getRGBFromColour(colours[i], rgb);
	r += rgb[0];
	g += rgb[1];
	b += rgb[2];
  }
  
  r /= size; r &= 255;
  g /= size; g &= 255;
  b /= size; b &= 255;
    
  return getColourFromRGB(r, g, b);
}