#include "object.hh"

Object::Object() { 
  for (int i = 0; i < 3; ++i) {
    position[i] = 0;
    rotation[i] = 0;
  }

  externalMaterial = false;
  material = new Material(75, 75, 75);
}

Object::Object(Material* m) {
  for (int i = 0; i < 3; ++i) {
    position[i] = 0;
    rotation[i] = 0;
  }

  externalMaterial = true;
  material = m;
}

Object::~Object() {
  if (!externalMaterial) {
    delete material;
  }
}

Collision* Object::collideWith(Ray& ray) {
  return nullptr;
}

void Object::getNormal(float* p, float* normal) {
  normal[0] = 0;
  normal[1] = 0;
  normal[2] = 0;
}

void Object::setPosition(float x, float y, float z) {
  position[0] = x;
  position[1] = y;
  position[2] = z;
}

void Object::setPosition(float* p) {
  this->setPosition(p[0], p[1], p[2]);
}

Material& Object::getMaterial() {
  return *material;
}

Material* Object::swapMaterial(Material* newMaterial) {
  Material* temp = material;
  material = newMaterial;
  return temp;
}


uint32_t Object::getColorAt(float* pos)
{
  float valeur = 255;

  uint8_t rgb[3];
  uint32_t col = 0;
  BMPImage::getRGBFromColour(getMaterial().colour, rgb); 

  switch(this->getMaterial().texture) {
     case None: 
      return getMaterial().colour; 
      break;
     case Board:
      col = getBoard(pos[0], pos[1]);
      return (col!=0?getMaterial().colour:col);
      break;
     case Perlin1: 
      valeur = (perlinnoise(pos[0], pos[1], pos[2], 5, 1)+1)*0.5*255;
      return BMPImage::getColourFromRGB(valeur, valeur, valeur);
      break;
    case Perlin2: 
      valeur = (perlinnoise(pos[0], pos[1], pos[2], 7, 0.5)+1)*0.5*255;  
      return BMPImage::getColourFromRGB(valeur, valeur, valeur); 
      break;
    case Perlin3: 
      valeur = (perlinnoise(pos[0], pos[1], pos[2], 5, 1)+1)*0.5;  
      return BMPImage::getColourFromRGB(rgb[0]*valeur, rgb[1]*valeur, rgb[2]*valeur); 
      break;
    case BandeSphere: 
      col = getBandeSphere(pos[0], pos[1]); 
      return (col!=0?getMaterial().colour:col);
      break;
  }

   return getMaterial().colour; 
}