#ifndef __PLANE__HH__
#define __PLANE__HH__

#include "object.hh"
#include "util.hh"
#include "collision.hh"
#include "perlinnoise.hh"

class Plane : public Object {
private:
  float origin[3];
  float normal[3];
  float d;

  void computeD();

public:

  Plane();
  ~Plane();

  void setNormal(float* f);
  void setOrigin(float* f);

  Collision* collideWith(Ray& ray);
  void getNormal(float*p, float* ansNormal);

  void getPosFrom(float* pos, float* newPos);

  float Get2DPerlinNoiseValue(float x, float y, float res);

  //static Plane getPlaneFromPoints(float* p1, float* p2, float* p3);
  //static Plane getPlaneFromVectors(float* p, float* v1, float* v2);

  bool whichSide(float* p);

  void print(std::ostream& o) {
    o << "N{";
    for(int i = 0; i < 3; ++i) {
      o << normal[i] << ((i < 2)?" ":"");
    }
    o << "} P{";
    for(int i = 0; i < 3; ++i) {
      o << origin[i] << ((i < 2)?" ":"");
    }
    o << "}";
  }
};

std::ostream& operator<< (std::ostream& o, Plane& p);

#endif
