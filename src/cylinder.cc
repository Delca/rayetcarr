#include "cylinder.hh"

Cylinder::Cylinder(float r, float l) {
  radius = r;
  length = l;
  initDirection();
}

Cylinder::Cylinder() : radius(1), length(1) {
  initDirection();
}

void Cylinder::initDirection() {
  capped = true;
  for (int i = 0; i < 3; ++i) {
    direction[i] = -(i == 2);
  }
}

Cylinder::~Cylinder() {}

Collision* Cylinder::collideWith(Ray& ray) {
  float distB[3] = {ray.getB()[0] - position[0],
		    ray.getB()[1] - position[1],
		    ray.getB()[2] - position[2]};

  float* u = direction;
  float* v = ray.getA();

  float crossUV[3] = {0, 0, 0};
  float crossUB[3] = {0, 0, 0};

  Util::cross(u, v, crossUV);
  Util::cross(u, distB, crossUB);

  float b = 2*Util::dot(crossUV, crossUB);
  float a = (std::pow(crossUV[0], 2)
	     + std::pow(crossUV[1], 2)
	     + std::pow(crossUV[2], 2));
  float c = (std::pow(crossUB[0], 2)
	     + std::pow(crossUB[1], 2)
	     + std::pow(crossUB[2], 2)) - std::pow(radius, 2);

  float delta = std::pow(b, 2) - 4*a*c;

  if (delta < 0) {
    // std::cout << "NEGATIVE DELTA " << delta << std::endl;
    return (capped?checkCaps(ray, nullptr):nullptr);
  }

  float t1 = (-b - std::sqrt(delta)) / (2 * a);
  float t2 = (-b + std::sqrt(delta)) / (2 * a);

  if (t1 < 0 && t2 < 0) {
    // Since t1 < t2, both intersection are
    // behind the origin of the ray
    return (capped?checkCaps(ray, nullptr):nullptr);
  }

  // If t1 is behind the origin, the intersection
  // we want is t2, since it is before us.
  // Else, we know that t1 is closer to us than t2.
  float t = (t1 < t2 ? t1 : t2);

  if (t < 0) {
    t = (t1 < t2 ? t2 : t1);
  }

  float intersection[3] = {ray.getA()[0] * t + ray.getB()[0],
			   ray.getA()[1] * t + ray.getB()[1],
			   ray.getA()[2] * t + ray.getB()[2]};

  if (capped && length > -1) {
    float proj[3] = {0, 0, 0};
    Util::projectPointLine(intersection, direction, position, proj);
    float dir[3] = {0, 0, 0};
    for(int i = 0; i < 3; ++i) {
      dir[i] = proj[i] - position[i];
    }

    if (Util::distance(proj, position) > length || Util::dot(dir, direction) < 0) {
      return (capped?checkCaps(ray, nullptr):nullptr);
    }
  }

  Collision* collision = new Collision(intersection, this);
  
  return (capped?checkCaps(ray, collision):collision);
}

void Cylinder::getNormal(float* p, float* normal) {
  float proj[3] = {0, 0, 0};
  Util::projectPointLine(p, direction, position, proj);

  for(int i = 0; i < 3; ++i) {
    normal[i] = p[i] - proj[i];
  }
  Util::normalize(normal);
}

Collision* Cylinder::checkCaps(Ray& ray, Collision* prevColl) {
  Collision* coll = prevColl;
  Plane p;
  float pos[3] = {position[0],
		  position[1],
		  position[2]};

  for (int i = 0; i < 3; ++i) {
    //pos[i] += length*direction[i];
  }

  p.setNormal(direction);
  p.setOrigin(pos);

  Collision* cap = p.collideWith(ray);

  if (cap) {
    float proj[3] = {0, 0, 0};

    Util::projectPointLine(cap->position, direction, position, proj);

    if (Util::distance(cap->position, proj) <= radius) {
      if (coll == nullptr || Util::distance(cap->position, ray.getB()) < Util::distance(coll->position, ray.getB())) {
	if (coll != nullptr) {
	  delete coll;
	}
	coll = cap;
	Util::reverse(coll->normal);
      }
    }
  }
  if (cap && coll != cap) {
    delete cap;
  }

  for (int i = 0; i < 3; ++i) {
    pos[i] += direction[i]*length;
  }
  p.setOrigin(pos);

  cap = p.collideWith(ray);

  if (cap) {
    float proj[3] = {0, 0, 0};

    Util::projectPointLine(cap->position, direction, position, proj);

    if (Util::distance(cap->position, proj) <= radius) {
      if (coll == nullptr || Util::distance(cap->position, ray.getB()) < Util::distance(coll->position, ray.getB())) {
	if (coll != nullptr) {
	  delete coll;
	}
	coll = cap;
	Util::reverse(coll->normal);
      }
    }
  }
  if (cap && coll != cap) {
    delete cap;
  }

  if (coll) {
    coll->object = this;
    coll->color = material->colour;
  }

  return coll;
}

void Cylinder::getPosFrom(float* pos, float* newPos)
{
}
