#include "plane.hh"

Plane::Plane() {
  for(int i = 0; i < 3; ++i) {
    origin[i] = 0;
    normal[i] = 0;
  }
  computeD();
}

Plane::~Plane() {}

void Plane::setNormal(float* f) {
  for(int i = 0; i < 3; ++i) {
    normal[i] = f[i];
  }
  computeD();
}

void Plane::setOrigin(float* f) {
  for(int i = 0; i < 3; ++i) {
    origin[i] = f[i];
  }
  computeD();
}

void Plane::computeD() {
  d = 0;
  for(int i = 0; i < 3; ++i) {
    d -= normal[i]*origin[i];
  }
}

void Plane::getNormal(float* p, float* ansNormal) {
  for(int i = 0; i < 3; ++i) {
    ansNormal[i] = normal[i];
  }
}

bool Plane::whichSide(float* p) {
  float result = d;

  for(int i = 0; i < 3; ++i) {
    result += normal[i] * p[i];
  }

  return result > 0;
}

Collision* Plane::collideWith(Ray& ray) {
  float dotA = Util::dot(normal, ray.getA());
  float dotB = Util::dot(normal, ray.getB());
  float t = -(dotB+d)/dotA;

  if (t < 0) {
    return nullptr;
  }

  float collision[3];

  for(int i = 0; i < 3; ++i) {
    collision[i] = ray.getA()[i]*t + ray.getB()[i];
  }

  float pos[3];
  getPosFrom(collision, pos);
  uint32_t color = getColorAt(pos);

  return new Collision(collision, this, color);
}


void Plane::getPosFrom(float* position, float* newPos)
{
   float a1[3];

    a1[0] = 0;
    a1[1] = -this->normal[2];
    a1[2] = this->normal[1];

    if (a1[1] == 0 && a1[2] == 0)
    {
      a1[0] = -this->normal[2];
      a1[1] = 0;
      a1[2] = this->normal[0];

      if (a1[0] == 0 && a1[2] == 0)
      {
        a1[0] = -this->normal[1];
        a1[1] = this->normal[0];
        a1[2] = 0;
      }
    }


    float a2[3];

    Util::cross(this->normal, a1, a2);

    float p1[3];
    float p2[3];
    Util::projectPointLine(position, a1, this->origin, p1);
    Util::projectPointLine(position, a2, this->origin, p2);


    float t = (p1[0] - this->origin[0])/ a1[0];

    if (a1[0] == 0)
    {
      t = (p1[1] - this->origin[1])/ a1[1];
      
      if (a1[1] == 0)
        t = (p1[2] - this->origin[2])/ a1[2];
    }

    float t2 = (p2[0] - this->origin[0])/ a2[0];

    if (a2[0] == 0)
    {
      t2 = (p2[0] - this->origin[0])/ a2[0];
      
      if (a2[1] == 0)
        t2 = (p2[2] - this->origin[2])/ a2[2];
    }

    newPos[0] = t;
    newPos[1] = t2;
    newPos[3] = 0;
}

std::ostream& operator<<(std::ostream& o, Plane& p) {
  p.print(o);
  return o;
}
