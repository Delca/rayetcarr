#include "world.hh"

World::World() {
  //ambientColour = BMPImage::getColourFromRGB(30, 40, 80);
  ambientColour = BMPImage::getColourFromRGB(0, 0, 0);
  //ambientColour = BMPImage::getColourFromRGB(255, 255, 255);
}
World::~World() {
  for (Camera* c : cameras) {
    delete c;
  }
}

Camera& World::addCamera(int w, int h) {
  cameras.push_back(new Camera(w, h));

  cameras[cameras.size()-1]->setWorld(*this);

  return *cameras[cameras.size()-1];
}

int World::addObject(Object& object) {
  objects.push_back(&object);

  return objects.size() - 1;
}

void World::setAmbientColour(uint32_t colour) {
  ambientColour = colour;
}

void World::setAmbientColour(uint8_t r, uint8_t g, uint8_t b) {
  setAmbientColour(BMPImage::getColourFromRGB(r, g, b));
}


uint32_t World::fireRay(Ray& ray, int recurse) {
  uint32_t ans = ambientColour;
  Collision* nearestCollision = nullptr;
  Collision* lastCollision = nullptr;

  for (Object* object : objects) {
    if (lastCollision && lastCollision != nearestCollision) {
      delete lastCollision;
    }
    if ((lastCollision = object->collideWith(ray)) != nullptr && Util::distance(ray.getB(), lastCollision->position) > 0) {
      if (nearestCollision == nullptr
	  || Util::distance(ray.getB(), lastCollision->position) < Util::distance(ray.getB(), nearestCollision->position)) {
	if (nearestCollision != nullptr) {
	  delete nearestCollision;
	}
	nearestCollision = lastCollision;
      }
    }
  }

  if (lastCollision && lastCollision != nearestCollision) {
    delete lastCollision;
  }

  if (nearestCollision != nullptr) {
    /*std::cout << nearestCollision->object->getMaterial().reflection << std::endl;
    ans = nearestCollision->object->getMaterial().colour;
    return ans;*/
    ans = searchForLight(nearestCollision);
    if (recurse > 0) {
      ans = BMPImage::addProportionalColours(ans, fireReflectedRay(ray, nearestCollision, recurse-1), nearestCollision->object->getMaterial().reflection);
    }
    delete nearestCollision;
  }

  return ans;
}


int World::addLight(Light& light) {
  lights.push_back(&light);

  return lights.size() - 1;
}

uint32_t World::searchForLight(Collision* collision) {
  float r = 0;
  float g = 0;
  float b = 0;
//  uint32_t materialColour = collision->object->getMaterial().colour;
  uint32_t materialColour = collision->color;
  uint8_t cR = (materialColour & (255 << 16))>>16;
  uint8_t cG = (materialColour & (255 << 8))>>8;
  uint8_t cB = materialColour & (255);

  for (Light* light : lights) {
    Ray ray;
    ray.from(collision->position).to(light->getPosition());
    float startRay[3] = {
      ray.getB()[0] + (float)0.002*ray.getA()[0],
      ray.getB()[1] + (float)0.002*ray.getA()[1],
      ray.getB()[2] + (float)0.002*ray.getA()[2]};
    ray.from(startRay);

    uint32_t lightColour = light->getMaterial().colour;
    uint8_t lR = (lightColour & (255 << 16))>>16;
    uint8_t lG = (lightColour & (255 << 8))>>8;
    uint8_t lB = lightColour & (255);

    Collision* lastCollision = nullptr;
    float distanceToLightSource = Util::distance(collision->position, light->getPosition());
    bool isObstructed = false;

    for (Object* object : objects) {
      if ((lastCollision = object->collideWith(ray)) != nullptr) {
	if (Util::distance(collision->position, lastCollision->position) < distanceToLightSource && lastCollision->object != collision->object) {
	  isObstructed = true;
	  break;
	}
	delete lastCollision;
	lastCollision = nullptr;
      }
    }

    if (lastCollision) {
      delete lastCollision;
      lastCollision = nullptr;
    }

    if (isObstructed) {
      //std::cout << "obstruction\n";  
      continue;
    }
    
    float inverseLightRay[3] = {-ray.getA()[0],-ray.getA()[1],-ray.getA()[2]};
    float projectionCoeff = -2*Util::dot(inverseLightRay, collision->normal);
    float reflectedLightRay[3];

    for(int i = 0; i < 3; ++i) {
      reflectedLightRay[i] = inverseLightRay[i] + projectionCoeff*collision->normal[i];
    }

    float diffuseContribution = std::abs(Util::dot(ray.getA(), collision->normal));
    float specularContribution = pow(Util::dot(reflectedLightRay, collision->normal), 50);

    if (diffuseContribution > 0) {
      diffuseContribution *= collision->object->getMaterial().kd;
      //std::cout << diffuseContribution << std::endl;
      r = std::min(r + cR * ((float)lR/255) * diffuseContribution, (float) 255);
      g = std::min(g + cG * ((float)lG/255) * diffuseContribution, (float) 255);
      b = std::min(b + cB * ((float)lB/255) * diffuseContribution, (float) 255);
    }

    if (specularContribution > 0) {
      specularContribution *= collision->object->getMaterial().ks;
      r = std::min(r + lR * specularContribution, (float) 255);
      g = std::min(g + lG * specularContribution, (float) 255);
      b = std::min(b + lB * specularContribution, (float) 255);
    }


  }

  uint32_t ans = BMPImage::getColourFromRGB((uint8_t)r, (uint8_t)g, (uint8_t)b);

  //std::cout << "Colour found " << r << " " << g << " " << b << std::endl;

  return ans;
}

uint32_t World::fireReflectedRay(Ray& incidentRay, Collision* collision, int recurse) {
  float* incidentA = incidentRay.getA();
  float projectionCoeff = -2*Util::dot(incidentA, collision->normal);
  float reflectedVector[3];

  for (int i = 0; i < 3; ++i) {
    reflectedVector[i] = incidentA[i] + projectionCoeff*collision->normal[i]; 
  }

  Ray reflectedRay;
  reflectedRay.to(reflectedVector).from(collision->position);

  float startRay[3] = {
    reflectedRay.getB()[0] + (float)0.001*reflectedVector[0],
    reflectedRay.getB()[1] + (float)0.001*reflectedVector[1],
    reflectedRay.getB()[2] + (float)0.001*reflectedVector[2]};
  reflectedRay.from(startRay);

  uint32_t reflectedIntensity = fireRay(reflectedRay, recurse);

  return reflectedIntensity;
}
