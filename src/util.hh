#ifndef __UTIL__HH__
#define __UTIL__HH__

#include <cmath>
#include <iostream>

class Util {
public:

  static float origin[3];

  static float distance(float* p1, float* p2) {
    return sqrt( pow(p2[0]-p1[0], 2) + pow(p2[1]-p1[1], 2) + pow(p2[2]-p1[2], 2) );
  }

  static float squaredNorm(float* p) {
    return dot(p, p);
  }

  static float dot(float* p1, float* p2) {
    return p1[0]*p2[0] + p1[1]*p2[1] + p1[2]*p2[2];
  }

  static float norm(float* p) {
    return sqrt(squaredNorm(p));
  }

  static void cross(float* u, float* v, float* cross) {
    cross[0] = u[1]*v[2] - u[2]*v[1];
    cross[1] = u[2]*v[0] - u[0]*v[2];
    cross[2] = u[0]*v[1] - u[1]*v[0];
  }

  static void projectPointLine(float* p, float* a, float* b, float* ans) {
    float point[3];
    
    for(int i = 0; i < 3; ++i) {
      point[i] = p[i] - b[i];
    }

    float t = Util::dot(a, point);

    for(int i = 0; i < 3; ++i) {
      ans[i] = a[i]*t + b[i];
    }

  }

  static void normalize(float* p) {
    float norm = Util::norm(p);

    for(int i = 0; i < 3; ++i) {
      p[i] /= norm;
    }
  }

  static void reverse(float* f) {
    for (int i = 0; i < 3; ++i) {
      f[i] *= -1;
    }
  }

};

#endif
