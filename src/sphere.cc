#include "sphere.hh"

Sphere::Sphere(float r) {
  radius = r;
}

Sphere::Sphere() : Sphere(1) {}
Sphere::~Sphere() {}

Collision* Sphere::collideWith(Ray& ray) {
  float* a = ray.getA();
  float* b = ray.getB();
  float projection[3] = {this->position[0]-b[0],
			 this->position[1]-b[1],
			 this->position[2]-b[2]};
  float coeff = Util::dot(a, projection) / Util::squaredNorm(a);

  projection[0] = coeff*a[0] + b[0];
  projection[1] = coeff*a[1] + b[1];
  projection[2] = coeff*a[2] + b[2];

  float distToRay = Util::distance(projection, this->position);

  if (distToRay - radius < 0.00001) {
    float dist = sqrt(radius*radius - distToRay*distToRay);

    for (int i = 0; i < 3; ++i) {
      projection[i] -= dist*a[i];
    }

    float d1 = Util::distance(projection, b);

    for (int i = 0; i < 3; ++i) {
      projection[i] += 2*dist*a[i];
    }

    float d2 = Util::distance(projection, b);

    if (d1 < d2 || computeCoeff(a, b, projection) < 0) {
      for (int i = 0; i < 3; ++i) {
	projection[i] -= 2*dist*a[i];
      }
    }

    if (computeCoeff(a, b, projection) < 0) {
      return nullptr;
    }

  //  uint32_t color = getColorAtPlace(projection);
    
    float pos[3];
    getPosFrom(projection, pos);
    uint32_t c = getColorAt(pos);

    return new Collision(projection, this, c);
  }

  return nullptr;
}

// If the returned value is less than 0,
// the point p is "behind" the origin b
// of the ray a*t + b
float Sphere::computeCoeff(float* a, float* b, float* p) {
  for (int i = 0; i < 3; ++i) {
    if (a[i] == 0) continue;
    return (p[i] - b[i])/a[i];
  }

  // Undefined behaviour, should not happen
  // under normal circumstances
  std::cout << "WHAT\n";
  return -1;
}

void Sphere::getNormal(float* p, float* normal) {
  normal[0] = p[0] - this->position[0];
  normal[1] = p[1] - this->position[1];
  normal[2] = p[2] - this->position[2];

  float norm = Util::norm(normal);

  normal[0] /= norm;
  normal[1] /= norm;
  normal[2] /= norm;
}

void Sphere::getPosFrom(float* pos, float* newPos)
{
  float phi = std::acos((pos[2]-position[2]) / this->radius);
  float theta = 0;


  if (pos[0] == position[0])
  {
    if (pos[1] < position[1])
      theta = -M_PI / 2;
    else
      theta = M_PI / 2;
  }
  else
    theta = std::atan((pos[1]-position[1]) / (pos[0]-position[0]));
 
  theta += M_PI / 2;


  theta /= M_PI;
  phi /= M_PI;

  newPos[0] = phi * 10;
  newPos[1] = theta * 10;
  newPos[2] = 0;
}