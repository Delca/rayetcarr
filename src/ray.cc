#include "ray.hh"

Ray::Ray() {
  for (int i = 0; i < 3; ++i) {
    a[i] = 0;
    b[i] = 0;
  }
}

Ray::~Ray() {}

Ray& Ray::from(float x, float y, float z) {
  b[0] = x;
  b[1] = y;
  b[2] = z;

  return *this;
}

Ray& Ray::to(float x, float y, float z) {
  a[0] = x - b[0];
  a[1] = y - b[1];
  a[2] = z - b[2];

  float total = Util::norm(a);
  a[0] = a[0]/total;
  a[1] = a[1]/total;
  a[2] = a[2]/total;

  return *this;
}

Ray& Ray::from(float* p) {
  return this->from(p[0], p[1], p[2]);
}

Ray& Ray::to(float* p) {
  return this->to(p[0], p[1], p[2]);
}

float* Ray::getA() {
  return a;
}

float* Ray::getB() {
  return b;
}

std::ostream& operator<<(std::ostream& o, Ray& ray) {
  float* a = ray.getA();
  float* b = ray.getB();

  o << "A{" << a[0] << "," << a[1] << "," << a[2]
    << "} B" << b[0] << "," << b[1] << "," << b[2];

  return o;
}
