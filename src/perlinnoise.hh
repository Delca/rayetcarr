#ifndef __PERLINNOISE__HH__
#define __PERLINNOISE__HH__

#include <array>
#include <cmath>
#include <cstdint>
#include "bmpWriter.hh"

double fade(double t);
double lerp(double t, double a, double b);
double gradient(int hash, double x, double y, double z);
double getnoise(double x, double y, double z);
double perlinnoise(double x, double y, double z, int oct, double amp);
uint32_t getBoard(float x, float y);
uint32_t getBandeSphere(float phi, float theta);
uint32_t getBoardSphere(float phi, float theta);

#endif