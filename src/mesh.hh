#ifndef __MESH__HH__
#define __MESH__HH__

#include <vector>
#include <limits>
#include <iostream>
#include <fstream>
#include <sstream>

#include "object.hh"
#include "ray.hh"
#include "collision.hh"
#include "plane.hh"

#define FAST_TRIANGLE

struct Triangle : Object {
private:
  float normal[3];

  void init();
  void computeEdges();
public:
  float* p1;
  float* p2;
  float* p3;
  float v12[3];
  float v13[3];
  float v23[3];

  Triangle();
  Triangle(Material* m);
  ~Triangle();

  void setPoints(float* v1, float* v2, float* v3);

  Collision* collideWith(Ray& ray);
  void getNormal(float* p, float* normal);

  void getPosFrom(float* position, float* newPos);
};

class Mesh : public Object {
private:
  std::vector<Triangle*> triangles;
  std::vector<float*> vertices;
  std::vector<Material*> materials;

public:
  Mesh();
  ~Mesh();

  int addVertex(float x, float y, float z);
  int addTriangle(int v1, int v2, int v3);

  Collision* collideWith(Ray& ray);
  void getNormal(float* p, float* normal);
  void setPosition(float x, float y, float z);

  static Mesh* readfromObj(std::string fileName);

  void getPosFrom(float* position, float* newPos);
};

#endif
