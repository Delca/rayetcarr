#ifndef __PARSER__HH__
#define __PARSER__HH__

#include <map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>

#include "world.hh"
#include "object.hh"
#include "light.hh"
#include "camera.hh"
#include "sphere.hh"
#include "mesh.hh"

class Parser {
private:
  
  World* w;
  std::map<std::string, Object*> objects;
  std::map<std::string, Light*> lights;
  std::map<std::string, Camera*> cameras;
  
  
public:
  
  Parser();
  ~Parser();
  
  void parse(std::string filename);  
  void createCamera(std::vector<std::string> tokens);
  void createPlane(std::vector<std::string> tokens);
  void createCylinder(std::vector<std::string> tokens);
  void changeProperty(std::vector<std::string> tokens);
  void lookAt(std::vector<std::string> tokens);
  void createSphere(std::vector<std::string> tokens);
  void createLight(std::vector<std::string> tokens);
  void changePosition(std::vector<std::string> tokens);
  void render(std::vector<std::string> tokens);
  void createMesh(std::vector<std::string> tokens);
  void changeParam(std::vector<std::string> tokens);
  void createTriangle(std::vector<std::string> tokens);

  
};

#endif