#include <string>
#include <iostream>
#include "bmpWriter.hh"
#include "camera.hh"
#include "world.hh"
#include "sphere.hh"
#include "plane.hh"
#include "util.hh"
#include "mesh.hh"
#include "cylinder.hh"
#include "material.hh"
#include "parser.hh"

int width = 300;
int height = 200;


int main_cylinder() {
  World world;

  Camera& camera = world.addCamera(width, height);
  camera.setPosition(-5, 0, -4);
  
  float normal[] = {0, 0, 1};
  float origin[] = {0, 2.5, 2};
  Plane p;
  p.setNormal(normal);
  p.setOrigin(origin);
  world.addObject(p);

  Cylinder c(1, 2);
  c.setPosition(0, 5, 0);
  c.getMaterial().colour = BMPImage::getColourFromRGB(255, 0, 255);
  c.getMaterial().ks = 0.4;
  c.getMaterial().kd = 0.7;
  c.getMaterial().reflection = 0.5;
  c.capped = true;
  world.addObject(c);
  
  camera.lookAt(c);
  camera.setPosition(-1.2, 4.2, -2.4);

  Sphere s1;
  s1.setPosition(-2, 6, 0);
  s1.getMaterial().colour = BMPImage::getColourFromRGB(170, 75, 170);
  world.addObject(s1);
  Sphere s2;
  s2.setPosition(1, 4, -2);
  s2.getMaterial().colour = BMPImage::getColourFromRGB(255, 255, 70);
  world.addObject(s2);
  
  
  Sphere s3(4);
  s3.setPosition(5.5, 10, -2);
  s3.getMaterial().colour = BMPImage::getColourFromRGB(255, 255, 70);
  world.addObject(s3);
  camera.lookAt(s3);
  

  Light l1;
  l1.setPosition(0, 0, -48);
  l1.getMaterial().setColour(160, 160, 160);
  world.addLight(l1);

  Light l2;
  l2.setPosition(-100, -100, 0);
  l2.getMaterial().setColour(160, 160, 160);
  world.addLight(l2);

  Light l3;
  l3.setPosition(0, -100, -100);
  l3.getMaterial().setColour(160, 160, 160);
  world.addLight(l3);

  camera.render();
}

int main_test() {
  World world;

  Camera& camera = world.addCamera(width, height);
  camera.setPosition(0.8, 1.5, -4);
  camera.setPosition(-5, 0, -4);

  float normal[] = {0, 0, 1};
  float origin[] = {0, 2.5, 2};
  Plane p;
  p.getMaterial().texture = Perlin3;
  p.setNormal(normal);
  p.setOrigin(origin);
  world.addObject(p);
  
 

  Sphere s1;
  s1.setPosition(1, 6, 0);
  s1.getMaterial().colour = BMPImage::getColourFromRGB(170, 75, 170);
  s1.getMaterial().texture = Board;
  world.addObject(s1);

  Sphere s2;
  s2.setPosition(1, 4, -2);
  s2.getMaterial().colour = BMPImage::getColourFromRGB(255, 255, 70);
  s2.getMaterial().texture = Perlin3;
  world.addObject(s2);

  Sphere s3;
  s3.setPosition(0, 2, -2);
  s3.getMaterial().colour = BMPImage::getColourFromRGB(255, 255, 70);
  s3.getMaterial().texture = BandeSphere;
  world.addObject(s3);

  camera.lookAt(s1);

  Light l1;
  l1.setPosition(0, 0, -48);
  l1.getMaterial().setColour(160, 160, 160);
  world.addLight(l1);

  Light l2;
  l2.setPosition(-100, -100, 0);
  l2.getMaterial().setColour(160, 160, 160);
  world.addLight(l2);

  Light l3;
  l3.setPosition(0, -100, -100);
  l3.getMaterial().setColour(160, 160, 160);
  world.addLight(l3);

  camera.render();
}

int main_godusHouse() {
  World world;

  Camera& camera = world.addCamera(width, height);
  camera.setPosition(10, 11, 27);

  Mesh* m = Mesh::readfromObj("models/godusHouse");
  m->getMaterial().reflection = 0.5;
  world.addObject(*m);
  
  float normal[] = {0, 0, 1};
  float origin[] = {0, 0, 2};
  Plane p;
  p.setNormal(normal);
  p.setOrigin(origin);
  world.addObject(p);

  Light l1;
  l1.setPosition(0, 0, -100);
  l1.getMaterial().setColour(160, 160, 160);
  world.addLight(l1);
  Light l2;
  l2.setPosition(0, 0, 100);
  world.addLight(l2);
  Light l3;
  l3.setPosition(100, 0, 0);
  world.addLight(l3);
  Light l4;
  l4.setPosition(-100, 0, 0);
  l4.getMaterial().setColour(160, 160, 160);
  world.addLight(l4);

  camera.lookAt(*m);

  camera.render();

  delete m;
}

int main_market() {
  World world;

  Camera& camera = world.addCamera(width, height);
  camera.setPosition(9, 3, 0);

  Mesh* m = Mesh::readfromObj("models/marketStandTrig");
  m->getMaterial().reflection = 0;
  m->setPosition(1.7, 0, 0);
  world.addObject(*m);
  
  float normal[] = {0, 0, 1};
  float origin[] = {0, 0, 2};
  Plane p;
  p.setNormal(normal);
  p.setOrigin(origin);
  world.addObject(p);

  Light l1;
  l1.setPosition(0, 0, -100);
  l1.getMaterial().setColour(160, 160, 160);
  world.addLight(l1);
  Light l2;
  l2.setPosition(0, 0, 100);
  //world.addLight(l2);
  Light l3;
  l3.setPosition(100, 0, 0);
  //world.addLight(l3);
  Light l4;
  l4.setPosition(-100, 0, 0);
  l4.getMaterial().setColour(160, 160, 160);
  world.addLight(l4);

  camera.lookAt(*m);

  camera.render();

  delete m;
}

int main3() {
  World world;

  Camera& camera = world.addCamera(300, 200);
  //camera.setPosition(3, -1, -4);

  float p1[] = {-1.5, 2, 1};
  float p2[] = {0, 2, -1};
  float p3[] = {1.5, 2, 1};

  Sphere s1(0.4);
  s1.getMaterial().setColour(255, 0, 0);
  world.addObject(s1);
  Sphere s2(0.4);
  s2.getMaterial().setColour(0, 255, 0);
  world.addObject(s2);
  Sphere s3(0.4);
  s3.getMaterial().setColour(0, 0, 255);
  world.addObject(s3);

  Triangle t;
  t.getMaterial().setColour(10, 75, 200);
  t.setPoints(p1, p2, p3);
  t.getMaterial().reflection = 0.5;
  world.addObject(t);
  
  Mesh m;
  m.getMaterial().setColour(10, 105, 200);
  
  m.addVertex(0, 1, 0);
  m.addVertex(1, 1, 0);
  m.addVertex(0, 1, 1);
  m.addVertex(1, 1, 1);

  m.addTriangle(0, 1, 2);
  m.addTriangle(3, 1, 2);

  world.addObject(m);

  s1.setPosition(p1);
  s2.setPosition(p2);
  s3.setPosition(p3);

  Light l1;
  l1.setPosition(0, 0, -100);
  world.addLight(l1);
  Light l2;
  l2.setPosition(0, -100, -10);
  world.addLight(l2);  

  //camera.lookAt(s1);

  camera.render();
}

int main2() {
  World world;

  Camera& camera = world.addCamera(300, 200);
  camera.setPosition(0, 0, 0);

  Sphere s1(1);
  s1.setPosition(-1.1, 2.5, 0);
  s1.getMaterial().colour = BMPImage::getColourFromRGB(10, 185, 45);
  s1.getMaterial().reflection = 0.4;
  world.addObject(s1);

  Sphere s2(0.5);
  s2.setPosition(0, 5.5, -1);
  s2.getMaterial().colour = BMPImage::getColourFromRGB(0, 0, 255);
  s2.getMaterial().reflection = 0.2;
  world.addObject(s2);

  Sphere s3(1);
  s3.setPosition(1.1, 2.5, 0);
  s3.getMaterial().colour = BMPImage::getColourFromRGB(185, 0, 185);
  s3.getMaterial().reflection = 0.4;
  world.addObject(s3);

  float z = -1.5;
  float p1[] = {-1.1, 2.5, z};
  float p2[] = {1.1, 2.5, z};
  float p3[] = {0, 2.5, z-1};

  float v1[] = {1, 0, 0};
  float v2[] = {0, 1, 0};

  
  Triangle t;
  //t.getMaterial().kd = 0.77;
  t.getMaterial().colour = BMPImage::getColourFromRGB(0, 255, 0);
  t.setPoints(p1, p2, p3);
  world.addObject(t);

  float normal[] = {0, 0, 1};
  float origin[] = {0, 2.5, 2};
  Plane p;
  p.setNormal(normal);
  p.setOrigin(origin);
  p.getMaterial().colour = BMPImage::getColourFromRGB(255, 255, 0);
  p.getMaterial().kd = 0.6;
  p.getMaterial().ks = 0.3;
  p.getMaterial().reflection = 0;
  world.addObject(p);



  Light l2;
  l2.getMaterial().colour = BMPImage::getColourFromRGB(255, 255, 255);
  l2.setPosition(0, -551.5, 0);
  //world.addLight(l2);

  Light l3;
  l3.getMaterial().colour = BMPImage::getColourFromRGB(255, 255, 255);
  l3.setPosition(0, -100, -10);
  world.addLight(l3);

  Light l4;
  l4.getMaterial().colour = BMPImage::getColourFromRGB(255, 255, 255);
  l4.setPosition(0, 0, -100);
  world.addLight(l4);

  camera.render();

}

int mainImage() {

  int height = 64;
  int width = 64;

  BMPImage image(width, height);

  for (int i = 0; i < height; ++i) {
    for (int j = 0; j < width; ++j) {
      image.pixels[j + i*width] = BMPImage::getColourFromRGB(16*i, 16*j, 16*(i-j));
    }
  }
    

  image.writeToFile("test.bmp");

  return 0;
}

int main(int argc, char** argv) {
  
  Parser p;
  
  if (argc > 1) {
      p.parse(argv[1]);
  }
  else {
   std::cerr << "Usage: " << argv[0] << "path/to/script/file" << std::endl;
   exit(1);
  }

}






