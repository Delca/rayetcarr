#include "material.hh"

Material::Material(uint8_t r, uint8_t g, uint8_t b) {
  colour = BMPImage::getColourFromRGB(r, g, b);
  kd = 0.7;
  ks = 0.4;
  reflection = 0;
  texture = None;
}

Material::Material() {}
Material::~Material() {}

void Material::setColour(uint8_t r, uint8_t g, uint8_t b) {
  this->colour = BMPImage::getColourFromRGB(r, g, b);
}

void Material::setTexture(Texture t) {
  this->texture = t;
}

std::ostream& operator<<(std::ostream& o, Material& m) {
  m.print(o);
  return o;
}
