#ifndef __WORLD__HH__
#define __WORLD__HH__

#include <vector>
#include <cstdint>
#include <cmath>

#include "camera.hh"
#include "bmpWriter.hh"
#include "object.hh"
#include "util.hh"
#include "light.hh"
#include "material.hh"
#include "cylinder.hh"

class Camera;

class World {
private:

  std::vector<Camera*> cameras;
  std::vector<Object*> objects;
  std::vector<Light*> lights;
  uint32_t ambientColour;

  uint32_t searchForLight(Collision* collision);
  uint32_t fireReflectedRay(Ray& incidentRay, Collision* collision, int recurse = 1);

public:

  World();
  ~World();

  void setAmbientColour(uint32_t colour);
  void setAmbientColour(uint8_t r, uint8_t g, uint8_t b);

  uint32_t fireRay(Ray& ray, int recurse=3);
  
  Camera& addCamera(int w, int h);
  int addObject(Object& object);
  int addLight(Light& light);

};

#endif
