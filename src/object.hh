#ifndef __OBJECT__HH__
#define __OBJECT__HH__

#include <cstdint>

#include "ray.hh"
#include "bmpWriter.hh"
#include "collision.hh"
#include "material.hh"
#include "perlinnoise.hh"

class Collision;

class Object {
private:
protected:

  float position[3];
  float rotation[3];
  Material* material;
  bool externalMaterial;
 

public:

  Object();
  Object(Material* m);
  ~Object();

  float* getPosition() {return position;}
  float* getRotation() {return rotation;}
  Material& getMaterial();

  Material* swapMaterial(Material* newMaterial);
  virtual void setPosition(float x, float y, float z);
  virtual void setPosition(float* p);

  virtual Collision* collideWith(Ray& ray);
  // The resulting normal will be stored
  // in the second argument.
  // WARNING : This method is to be
  // called only by a Collision object.
  virtual void getNormal(float* p, float* normal);
 
  virtual void getPosFrom(float* pos, float* newPos) = 0;
  uint32_t getColorAt(float* pos);

};

#endif
