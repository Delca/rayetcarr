#ifndef __CAMERA__HH__
#define __CAMERA__HH__

#include <string>
#include <ctime>
#include <thread>
#include <mutex>

#include "bmpWriter.hh"
#include "ray.hh"
#include "world.hh"
#include "object.hh"

class World;

class Camera {
private:

  int width;
  int height;
  BMPImage image;

  float position[3];
  
  float focal;

  float pixelWidth;
  float pixelHeight;

  World* world = nullptr;

  float XAxis[3];
  float YAxis[3];
  float ZAxis[3];

  bool fileWrite;
  void displaceInScreenSpace(float* point, float* displ);

  void trace();

public:
  
  static float eyeSpace;
  static int maxSampling;
  static int numOfThread;
  
  Camera(int w, int h);
  Camera();
  ~Camera();

  float* getPosition() {return position;}

  void setPosition(float x, float y, float z);
  void setFocal(float f);
  void setPixelSize(int w, int h);
  void setWorld(World& w);
  
  void forward(float y);

  void lookAt(float* p);
  void lookAt(Object& o);

  void render();
  void render(std::string filename);

  void anaglyph();
  void anaglyph(std::string filename);

  void stereogram();
  void stereogram(std::string filename);

};

#endif
