#ifndef __BMP_WRITER__HH__
#define  __BMP_WRITER__HH__

#include <string>
#include <iostream>
#include <fstream>
#include <cstdint>

class BMPImage {

private:
  int width;
  int height;

public:

  uint32_t* pixels = nullptr;

  BMPImage() = delete;
  BMPImage(int w, int h);
  ~BMPImage();

  void writeToFile(std::string filename);
  void writeColour(int i, int j, uint32_t colour);
  uint32_t* switchBuffer(bool realloc = true);
  void setBuffer(uint32_t* buffer);

  static uint32_t getColourFromRGB(uint8_t r, uint8_t g, uint8_t b);
  static uint32_t getColourFromRGB(uint8_t* rgb);
  static uint32_t addColours(uint32_t c1, uint32_t c2);
  static uint32_t multiplyColours(uint32_t c1, uint32_t c2);
  static void getRGBFromColour(uint32_t colour, uint8_t* rgb);
  static uint32_t addProportionalColours(uint32_t c1, uint32_t c2, float perc);
  static uint32_t meanColour(uint32_t* colours, int size);
  
};

#endif
