#include "camera.hh"

void Camera::displaceInScreenSpace(float* point, float* displ) {
  for(int i = 0; i < 3; ++i) {
    point[i] += displ[0]*XAxis[i];
  }

  for(int i = 0; i < 3; ++i) {
    point[i] += displ[1]*YAxis[i];
  }

  for(int i = 0; i < 3; ++i) {
    point[i] += displ[2]*ZAxis[i];
  }
}

void Camera::forward(float y) {
  for(int i = 0; i < 3; ++i) {
    position[i] += y*YAxis[i];
  }
}

int Camera::maxSampling = 1;
int Camera::numOfThread = 1;

Camera::Camera(int w, int h) : image(w, h) {
  width = w;
  height = h;

  for (int i = 0; i < 3; ++i) {
    position[i] = 0;
    XAxis[i] = 0;
    YAxis[i] = 0;
    ZAxis[i] = 0;
  }
  XAxis[0] = 1;
  YAxis[1] = 1;
  ZAxis[2] = 1;

  focal = 10;
  pixelWidth = .1;
  pixelHeight = .1;
  
  world = nullptr;

  fileWrite = true;
}

Camera::Camera() :
  Camera(300, 200) 
{
  fileWrite = true;
}

Camera::~Camera() {

}

void Camera::setPosition(float x, float y, float z) {
  position[0] = x;
  position[1] = y;
  position[2] = z;
}

void Camera::setFocal(float f) {
  focal = f;
}

void Camera::setPixelSize(int w, int h) {
  pixelWidth = w;
  pixelHeight = h;
}

void Camera::setWorld(World& w) {
  world = &w;
}

void Camera::render() {
  this->render("test.bmp");
}

void Camera::render(std::string filename) {
  this->trace();
  image.writeToFile(filename);
}

void Camera::trace() {
  if (!world) {
    std::cerr << "This camera does not belong to any world !" << std::endl;
    return;
  }

  clock_t startCPU = clock();
  struct timespec startReal;
  clock_gettime(CLOCK_MONOTONIC, &startReal);

  // Determine the in-universe screen resolution
  float sWidth = width * pixelWidth;
  float sHeight = height * pixelHeight;

  // Determine the center of the screen
  // The camera is pointed along the Y-axis
  float screenCenter[3] = {};
  for(int i = 0; i < 3; ++i) {
    screenCenter[i] = focal * YAxis[i];
  }

  float realScreenCenter[3] = {position[0] + screenCenter[0],position[1] + screenCenter[1],position[2] + screenCenter[2]};

  int processed = 0;
  std::cout << "Rendering" << std::flush;

  float pixelPosPARENT[3] = {
    -sWidth/2 + pixelWidth/2,
    0,
    sHeight/2 - pixelHeight/2
  };

  std::mutex mtx;

  auto lambda = [this, &screenCenter, &processed, sHeight, &mtx, &pixelPosPARENT](int threadId){
    float pixelPos[3] = {pixelPosPARENT[0] + threadId*pixelWidth, pixelPosPARENT[1], pixelPosPARENT[2]};
    float realPixelPos[3] = {0, 0, 0};
    uint32_t colours[256] = {};
    float rayStart[3] = {this->position[0], this->position[1], this->position[2]};
    float rayTarget[3] = {};
    
    float tempDiff[3] = {};
    
    for(int i = threadId; i < this->width; i += numOfThread) {
      for (int j = 0; j < this->height; ++j) {      
	Ray ray;
      
      
      for(int rS = 0; rS < maxSampling; ++rS) {
      
	float randX = (float) std::rand() / RAND_MAX;
	float randZ = (float) std::rand() / RAND_MAX;
	
	randX -= 0.5;
	randZ -= 0.5;
	
	if (maxSampling == 1) {
	  randX = 0; randZ = 0;
	}
		  
	for(int k = 0; k < 3; ++k) {
	  realPixelPos[k] = screenCenter[k];
	}
	
	
	float modifiedPixelPos[3] = {pixelPos[0] + randX*pixelWidth, pixelPos[1], pixelPos[2] + randZ*pixelHeight};
	displaceInScreenSpace(realPixelPos, modifiedPixelPos);

	
	for(int k = 0; k < 3; ++k) {
	  rayTarget[k] = realPixelPos[k];
	}
	
	ray.from(rayStart);
	ray.to(rayTarget);
	
	colours[rS] = this->world->fireRay(ray);


      }

      this->image.writeColour(j, i, BMPImage::meanColour(colours, maxSampling));
      
      pixelPos[2] -= pixelHeight;

      ++processed;
      if (processed%(width*height/10) == 0) {
	std::cout << "." << std::flush;
	processed = 0;
      }
    }

    pixelPos[2] += sHeight;
    pixelPos[0] += numOfThread*pixelWidth;
    }
  };

  std::thread* threads = new std::thread[numOfThread];

  for(int i = 0; i < numOfThread; ++i) {
    threads[i] = std::thread(lambda, i);
  }

  for(int i = 0; i < numOfThread; ++i) {
    threads[i].join();
    }

  /*for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {      
      Ray ray;

      for(int k = 0; k < 3; ++k) {
  	realPixelPos[k] = screenCenter[k];
      }

      displaceInScreenSpace(realPixelPos, pixelPos);

      ray.from(position).to(realPixelPos);

      image.writeColour(j, i, world->fireRay(ray));

      pixelPos[2] -= pixelHeight;

      ++processed;
      if (processed%(width*height/10) == 0) {
  	std::cout << "." << std::flush;
  	processed = 0;
      }
    }

    pixelPos[2] += sHeight;
    pixelPos[0] += pixelWidth;
    }*/

  clock_t endCPU = clock();
  struct timespec endReal;
  clock_gettime(CLOCK_MONOTONIC, &endReal);


  std::cout << std::endl 
	    << "Tracing done in " << (endReal.tv_sec - startReal.tv_sec) + ((float)(endReal.tv_nsec - startReal.tv_nsec)/1000000000)
	    << " (CPU time: " << ((float)endCPU - startCPU)/CLOCKS_PER_SEC << "s)" << std::endl;

}

void Camera::lookAt(float* p) {
  float up[3] = {0, 0, -1};

  for(int i = 0; i < 3; ++i) {
    YAxis[i] = p[i] - position[i];
  }
  Util::normalize(YAxis);

  Util::cross(up, YAxis, XAxis);
  Util::normalize(XAxis);

  Util::cross(XAxis, YAxis, ZAxis);
  Util::normalize(ZAxis);

}

void Camera::lookAt(Object& o) {
  lookAt(o.getPosition());
}

void Camera::anaglyph() {
  this->anaglyph("test.bmp");
}

void Camera::anaglyph(std::string filename) {
  float posSave[3] = {position[0], position[1], position[2]};

  for(int i = 0; i < 3; ++i) {
    position[i] -= (eyeSpace/2) * XAxis[i];
  }

  trace();

  uint32_t* leftBuffer = image.switchBuffer();

  for(int i = 0; i < 3; ++i) {
    position[i] += eyeSpace * XAxis[i];
  }

  trace();

  uint32_t* rightBuffer = image.switchBuffer();

  // Combine buffers into the left one

  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      uint8_t rgbLeft[3] = {0, 0, 0};
      uint8_t rgbRight[3] = {0, 0, 0};

      BMPImage::getRGBFromColour(leftBuffer[i + j*width], rgbLeft);
      BMPImage::getRGBFromColour(rightBuffer[i + j*width], rgbRight);

      leftBuffer[i + j*width] =
	BMPImage::getColourFromRGB(rgbLeft[0],
				   0,//(rgbLeft[1] + rgbRight[1])/2,
				   rgbRight[2]);
    }
  }

  image.setBuffer(leftBuffer);

  for(int i = 0; i < 3; ++i) {
    position[i] = posSave[i];
  }

  image.writeToFile(filename);
}

void Camera::stereogram() {
  this->stereogram("test.bmp");
}

void Camera::stereogram(std::string filename) {
  BMPImage doubleImage(2*width, height);

  float posSave[3] = {position[0], position[1], position[2]};

  for(int i = 0; i < 3; ++i) {
    position[i] -= (eyeSpace/2) * XAxis[i];
  }

  trace();

  uint32_t* leftBuffer = image.switchBuffer(false);

  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      doubleImage.writeColour(j, i, leftBuffer[i + j*width]);
    }
  }

  for(int i = 0; i < 3; ++i) {
    position[i] += eyeSpace * XAxis[i];
  }

  trace();

  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      doubleImage.writeColour(j, i + width, leftBuffer[i + j*width]);
    }
  }

  doubleImage.writeToFile(filename);
}


float Camera::eyeSpace = 0.5;
