#ifndef __SPHERE__HH__
#define __SPHERE__HH__

#include <cmath>

#include "object.hh"
#include "collision.hh"
#include "util.hh"

class Sphere : public Object {
private:

  float radius;

  float computeCoeff(float* a, float* b, float* p);

public:

  Sphere(float r);
  Sphere();
  ~Sphere();

  Collision* collideWith(Ray& ray);
  void getNormal(float* p, float* normal);
  void getPosFrom(float* pos, float* newPos);

};

#endif
