#ifndef __COLLISION__HH__
#define __COLLISION__HH__

#include "object.hh"

class Object;

class Collision {
private:

public:

  float position[3];
  float normal[3];
  Object* object;
  uint32_t color;

  Collision(float* p, Object* o);
  Collision(float* p, Object* o, float* n);
  Collision(float* p, Object* o, uint32_t c);
  ~Collision();  

};

std::ostream& operator<<(std::ostream& o, Collision& collision);



#endif
