#include "perlinnoise.hh"

double fade(double t)
{
    return t * t * t * (t * (t * 6 - 15) + 10);
}
 
double lerp(double t, double a, double b)
{
    return a + t * (b - a);
}
 
double gradient(int hash, double x, double y, double z)
{
    int h = hash & 15;
    double u = h < 8 ? x : y;
    double v = h < 4 ? y : h == 12 || h == 14 ? x : z;

    return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}
 
double getnoise(double x, double y, double z) {
  
    unsigned int p[] =
       {151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,
        142,8,99,37,240,21,10,23,190,6,148,247,120,234,75,0,26,197,62,94,252,219,
        203,117,35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,168,68,175,
        74,165,71,134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,220,
        105,92,41,55,46,245,40,244,102,143,54,65,25,63,161,1,216,80,73,209,76,132,
        187,208,89,18,169,200,196,135,130,116,188,159,86,164,100,109,198,173,186,3,
        64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,
        47,16,58,17,182,189,28,42,223,183,170,213,119,248,152,2,44,154,163,70,221,
        153,101,155,167,43,172,9,129,22,39,253,19,98,108,110,79,113,224,232,178,185,
        112,104,218,246,97,228,251,34,242,193,238,210,144,12,191,179,162,241,81,51,145,
        235,249,14,239,107,49,192,214,31,181,199,106,157,184,84,204,176,115,121,50,45,
        127,4,150,254,138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,
        156,180};


    int32_t X = static_cast<int32_t>(std::floor(x)) & 255;
    int32_t Y = static_cast<int32_t>(std::floor(y)) & 255;
    int32_t Z = static_cast<int32_t>(std::floor(z)) & 255;
 
    x -= std::floor(x);
    y -= std::floor(y);
    z -= std::floor(z);
 
    double u = fade(x);
    double v = fade(y);
    double w = fade(z);
    

    int a = p[X % 256] + Y;
    int a2 = p[a % 256] + Z;
    int a3 = p[(a + 1) % 256] + Z;
    int b = p[(X + 1) % 256] + Y;
    int b2 = p[b % 256] + Z;
    int b3 = p[(b + 1) % 256] + Z;
 
    double pa2 = p[a2 % 256];
    double pb2 = p[b2 % 256];
    double pa3 = p[a3 % 256];
    double pb3 = p[b3 % 256];
    double pa21 = p[(a2 + 1) % 256];
    double pb21 = p[(b2 + 1) % 256];
    double pa31 = p[(a3 + 1) % 256];
    double pb31 = p[(b3 + 1) % 256];
 
    double aa = lerp(v, lerp(u, gradient(pa2, x, y, z), gradient(pb2, x-1, y, z)), lerp(u, gradient(pa3, x, y-1, z), gradient(pb3, x-1, y-1, z)));
    double bb = lerp(v, lerp(u, gradient(pa21, x, y, z-1), gradient(pb21, x-1, y, z-1)), lerp(u, gradient(pa31, x, y-1, z-1), gradient(pb31, x-1, y-1, z-1)));
 
    return lerp(w, aa, bb);
}

double perlinnoise(double x, double y, double z, int oct, double ampl) {
    double result = 0.0;
    double amp = ampl;

    int i = oct;

    for (int i = oct; i > 0; --i)
    {
        result += getnoise(x, y, z) * amp;
        x *= 2.0;
        y *= 2.0;
        z *= 2.0;
        amp *= 0.5;
    }
 
    return result;
}

uint32_t getBoard(float x, float y)
{
    if ((int) std::round(x) % 2 == 0)
    {

      if ((int) std::round(y) % 2 == 0)
        return BMPImage::getColourFromRGB(255, 255, 255);
      else
        return BMPImage::getColourFromRGB(0, 0, 0);
    }
    else
    {
       if ((int) std::round(y) % 2 == 0)
        return BMPImage::getColourFromRGB(0, 0, 0);
      else
        return BMPImage::getColourFromRGB(255, 255, 255);
    }

}

uint32_t getBandeSphere(float phi, float theta)
{
  
    if ((int) (phi) % 2 == 0)
       return BMPImage::getColourFromRGB(0, 0, 0);
    else
       return BMPImage::getColourFromRGB(255, 255, 255);  
}

uint32_t getBoardSphere(float phi, float theta)
{
    if ((int) (theta*10) % 2 == 0)
    {
        if ((int) (phi*10) % 2 == 0)
           return BMPImage::getColourFromRGB(255, 0, 0);
        else
           return BMPImage::getColourFromRGB(0, 255, 0);
    }
    else
    {
         if ((int) (phi*10) % 2 == 0)
           return BMPImage::getColourFromRGB(0, 255, 0);
        else
           return BMPImage::getColourFromRGB(255, 0, 0);
    }
    
}