#ifndef __MATERIAL__HH__
#define __MATERIAL__HH__

#include <cstdint>

#include "bmpWriter.hh"
#include "util.hh"


enum Texture {None, Board, Perlin1, Perlin2, Perlin3, BandeSphere};


class Material {
private:

public:
  
  uint32_t colour;
  float kd;
  float ks;
  float reflection;
  Texture texture;

  Material(uint8_t r, uint8_t g, uint8_t b);
  Material();
  ~Material();

  void setColour(uint8_t r, uint8_t g, uint8_t b);
  void setTexture(Texture t);

  void print(std::ostream& o) {
    uint8_t r = (colour & (255 << 16)) >> 16;
    uint8_t g = (colour & (255 << 8)) >> 8;
    uint8_t b = (colour & 255);

    o << "C(" << (int)r << " " << (int)g << " " << (int)b << ") kd " << kd << " ks " << ks << " refl " << reflection;
  }
};

std::ostream& operator<<(std::ostream& o, Material& m);

#endif
