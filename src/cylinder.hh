#ifndef __CYLINDER__HH__
#define __CYLINDER__HH__

#include <cmath>

#include "object.hh"
#include "collision.hh"
#include "util.hh"
#include "plane.hh"

class Cylinder : public Object {
private:

  float radius;
  float direction[3];
  float length;
  

  void initDirection();

  Collision* checkCaps(Ray& ray, Collision* coll);

public:

  bool capped;
  
  Cylinder(float r, float l);
  Cylinder();
  ~Cylinder();

  Collision* collideWith(Ray& ray);
  void getNormal(float* p, float* normal);

  void setDirection(float x, float y, float z) {
   direction[0] = x; 
   direction[1] = y;
   direction[2] = z;
  }
  void setLength(float le) {
   this->length = le; 
  }
  
  void getPosFrom(float* pos, float* newPos);

};

#endif
