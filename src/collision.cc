#include "collision.hh"

Collision::Collision(float* p, Object* o) {
  object = o;

  for(int i = 0; i < 3; ++i) {
    position[i] = p[i];
  }

  o->getNormal(p, this->normal);
  
  color = o->getMaterial().colour;
}

Collision::Collision(float* p, Object* o, uint32_t c) {
  object = o;

  for(int i = 0; i < 3; ++i) {
    position[i] = p[i];
  }

  o->getNormal(p, this->normal);

  color = c;
}

Collision::Collision(float* p, Object* o, float* n) {
  object = o;

  for(int i = 0; i < 3; ++i) {
    position[i] = p[i];
    normal[i] = n[i];
  }
  
  color = o->getMaterial().colour;
}

Collision::~Collision() {}

std::ostream& operator<<(std::ostream& o, Collision& collision) {
  float* position = collision.position;
  float* normal = collision.normal;
  
  o << "P{" << position[0] << "," << position[1] << "," << position[2]
    << "} B{" << normal[0] << "," << normal[1] << "," << normal[2] << "}";

  return o;
}
