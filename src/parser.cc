#include "parser.hh"

Parser::Parser() {
  w = new World();
}

Parser::~Parser() {
  delete w; 
}

std::vector<std::string> tokenize(std::string line) {
  std::vector<std::string> ans;
  
  std::istringstream iss(line);
  std::string token;
  
  while(std::getline(iss, token, ' ')) {
    ans.push_back(token); 
  }
  
  return ans;
}

void Parser::parse(std::string filename) {
  
  std::string line;
  std::ifstream file;
  file.open(filename);
  
  if (!file.is_open()) {
    std::cerr << "Could not open file " << filename << std::endl;
    exit(1);
  }
 
  std::vector<std::string> tokens;
 
  while (std::getline(file, line)) {
    tokens = tokenize(line);
    
    for(auto i : tokens) {
	std::cout << i << " ";
    }
    std::cout << std::endl;
    
    if (tokens.size() <= 0) continue;

    if (tokens[0] == "camera") {
      createCamera(tokens);
    }
    if (tokens[0] == "plane") {
      createPlane(tokens);
    }
    if (tokens[0] == "cylinder") {
      createCylinder(tokens);
    }
    if (tokens[0] == "material") {
      changeProperty(tokens);
    }
    if (tokens[0] == "lookAt") {
      lookAt(tokens);
    }
    if (tokens[0] == "sphere") {
      createSphere(tokens);
    }
    if (tokens[0] == "light") {
      createLight(tokens);
    }
    if (tokens[0] == "position") {
      changePosition(tokens);
    }
    if (tokens[0] == "render") {
      render(tokens);
    }
    if (tokens[0] == "mesh") {
      createMesh(tokens);
    }
    if (tokens[0] == "param") {
      changeParam(tokens);
    }
    if (tokens[0] == "triangle") {
      createTriangle(tokens);
    }
    
  }
  
  
  file.close();  
}

void Parser::createCamera(std::vector<std::string> tokens) {
  Camera* cam = &(w->addCamera(300, 200));
  cam->setPosition(std::atof(tokens[2].c_str()), std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()));
  
  cameras[tokens[1]] = cam; 
}

void Parser::createPlane(std::vector<std::string> tokens) {
  Plane* p = new Plane();
  
  
  float normal[] = {(float)std::atof(tokens[2].c_str()),
		    (float)std::atof(tokens[3].c_str()), (float)std::atof(tokens[4].c_str())};
  float origin[] = {(float)std::atof(tokens[5].c_str()),
		    (float)std::atof(tokens[6].c_str()), (float)std::atof(tokens[7].c_str())};
  p->setNormal(normal);
  p->setOrigin(origin);
  
  objects[tokens[1]] = p;
  
  w->addObject(*p);
}

void Parser::createCylinder(std::vector<std::string> tokens) {
  Cylinder* c = new Cylinder((float)std::atof(tokens[2].c_str()),
			     (float)std::atof(tokens[3].c_str()));
  
  c->setPosition(std::atof(tokens[4].c_str()),
		 std::atof(tokens[5].c_str()), std::atof(tokens[6].c_str()));
  
  if (tokens.size() > 7) {
    c->setDirection(std::atof(tokens[7].c_str()),
		 std::atof(tokens[8].c_str()), std::atof(tokens[9].c_str()));
  }
  
  if (tokens.size() > 10 && tokens[10] == "infinite") {
      c->capped = false;
      c->setLength(-1);
  }
  
  objects[tokens[1]] = c;
  
  w->addObject(*c);
}

void Parser::changeProperty(std::vector<std::string> tokens) {
  Object* o = (objects.find(tokens[1]) != objects.end()?objects[tokens[1]]:nullptr);
  Camera* c = (cameras.find(tokens[1]) != cameras.end()?cameras[tokens[1]]:nullptr);
  Light* l = (lights.find(tokens[1]) != lights.end()?lights[tokens[1]]:nullptr);
  
  if (tokens[2] == "colour" || tokens[2] == "color") {
    if (o != nullptr) {
      o->getMaterial().colour = BMPImage::getColourFromRGB(
	(uint8_t) std::atoi(tokens[3].c_str()),
	(uint8_t) std::atoi(tokens[4].c_str()),
	(uint8_t) std::atoi(tokens[5].c_str())
	);
      std::cout << "Changed colour of " << tokens[1]
      << (int)(uint8_t) std::atoi(tokens[3].c_str())<<
	(int)(uint8_t) std::atoi(tokens[4].c_str())<<
	(int)(uint8_t) std::atoi(tokens[5].c_str())<< std::endl;
    }
    if (l != nullptr) {
      l->getMaterial().colour = BMPImage::getColourFromRGB(
	(uint8_t) std::atoi(tokens[3].c_str()),
	(uint8_t) std::atoi(tokens[4].c_str()),
	(uint8_t) std::atoi(tokens[5].c_str())
	);
    }
  }
  
  if (tokens[2] == "ks") {
    if (o != nullptr) {
      o->getMaterial().ks = std::atof(tokens[3].c_str());
    }
  }
  
  if (tokens[2] == "kd") {
    if (o != nullptr) {
      o->getMaterial().kd = std::atof(tokens[3].c_str());
    }
  }
  
  if (tokens[2] == "reflection") {
    if (o != nullptr) {
      o->getMaterial().reflection = std::atof(tokens[3].c_str());
    }
  }
  
  if (tokens[2] == "texture") {
    if (o != nullptr) {
      
      if (tokens[3] == "None") {
	o->getMaterial().texture = None;
      }
      if (tokens[3] == "Board") {
	o->getMaterial().texture = Board;
      }
      if (tokens[3] == "Perlin1") {
	o->getMaterial().texture = Perlin1;
      }
      if (tokens[3] == "Perlin2") {
	o->getMaterial().texture = Perlin2;
      }
      if (tokens[3] == "Perlin3") {
	o->getMaterial().texture = Perlin3;
      }
      if (tokens[3] == "BandeSphere") {
	o->getMaterial().texture = BandeSphere;
      }
      
    }
  }
  
}

void Parser::lookAt(std::vector<std::string> tokens) {
  if (cameras.find(tokens[1]) != cameras.end()) {
    if (objects.find(tokens[2]) != objects.end()) {
      cameras[tokens[1]]->lookAt(*objects[tokens[2]]);
    }
  }  
}

void Parser::createSphere(std::vector<std::string> tokens) {
  Sphere* s = new Sphere((float)std::atof(tokens[2].c_str()));
  
  s->setPosition(std::atof(tokens[3].c_str()),
		 std::atof(tokens[4].c_str()), std::atof(tokens[5].c_str()));
  
  objects[tokens[1]] = s;
  
  w->addObject(*s);
}

void Parser::createLight(std::vector<std::string> tokens) {
  Light* l = new Light();
  l->setPosition(std::atof(tokens[2].c_str()), std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()));
  
  lights[tokens[1]] = l;
  
  w->addLight(*l);
}

void Parser::changePosition(std::vector<std::string> tokens) {
  Object* o = (objects.find(tokens[1]) != objects.end()?objects[tokens[1]]:nullptr);
  Camera* c = (cameras.find(tokens[1]) != cameras.end()?cameras[tokens[1]]:nullptr);
  Light* l = (lights.find(tokens[1]) != lights.end()?lights[tokens[1]]:nullptr);
  
  if (o != nullptr) {
    o->setPosition(std::atof(tokens[2].c_str()), std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()));
  }
  if (c != nullptr) {
    c->setPosition(std::atof(tokens[2].c_str()), std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()));
  }
  if (l != nullptr) {
    l->setPosition(std::atof(tokens[2].c_str()), std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()));
  }
  
}

void Parser::render(std::vector<std::string> tokens) {
  if (tokens.size() <= 0) {
    std::cerr << "No camera specified for rendering" << std::endl;
    exit(1);
  }
  
  if (cameras.find(tokens[1]) == cameras.end()) {
    std::cerr << "The camera " << tokens[1] << " was not found" << std::endl;
    exit(1);
  }
  
  std::string fileName = "out.bmp";
  
  if (tokens.size() > 2) {
    fileName = tokens[2];
  }
  
  if (tokens.size() > 3) {
    if (tokens[3] == "anaglyph") {
      cameras[tokens[1]]->anaglyph(fileName);
      return;
    }
    if (tokens[3] == "stereogram") {
      cameras[tokens[1]]->stereogram(fileName);
      return;
    }
  }
  
  cameras[tokens[1]]->render(fileName);
  
}

void Parser::createMesh(std::vector<std::string> tokens) {
  Mesh* m = Mesh::readfromObj("models/" + tokens[2]);
  
  std::cout << "Mesh read" << std::endl;
  
  m->setPosition(std::atof(tokens[3].c_str()),
		 std::atof(tokens[4].c_str()), std::atof(tokens[5].c_str()));
  
  objects[tokens[1]] = m;
  
  w->addObject(*m);
}

void Parser::changeParam(std::vector<std::string> tokens) {
  if (tokens[1] == "threads") {
    Camera::numOfThread = std::atoi(tokens[2].c_str());
  }
  
  if (tokens[1] == "samples") {
    std::cout << "HI" << std::endl;
    Camera::maxSampling = std::atoi(tokens[2].c_str());
  }
  
}

void Parser::createTriangle(std::vector<std::string> tokens) {
  float* p1 = (float*) malloc(3 * sizeof(float));
  float* p2 = (float*) malloc(3 * sizeof(float));
  float* p3 = (float*) malloc(3 * sizeof(float));
  
  for (int i = 0; i < 3; ++i) {
    p1[i] = (float)std::atof(tokens[2 + i].c_str());
    p2[i] = (float)std::atof(tokens[5 + i].c_str());
    p3[i] = (float)std::atof(tokens[8 + i].c_str());
  }

  Triangle* t = new Triangle();

  t->setPoints(p1, p2, p3);
  t->setPosition(0, 0, 0);

  objects[tokens[1]] = t;
  
  w->addObject(*t);
}




