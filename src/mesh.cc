#include "mesh.hh"

void Triangle::init() {
  normal[0] = 0;
  normal[1] = 1;
  normal[2] = 0;
}

Triangle::Triangle() {
  init();
}

Triangle::Triangle(Material* m) : Object(m) {
  init();
}

Triangle::~Triangle() {}

void Triangle::computeEdges() {
  for(int i = 0; i < 3; ++i) {
    this->v12[i] = p2[i] - p1[i];
    this->v13[i] = p3[i] - p1[i];
    this->v23[i] = p3[i] - p2[i];
  }
}

void Triangle::setPoints(float* v1, float* v2, float* v3) {
  p1 = v1;
  p2 = v2;
  p3 = v3;
  computeEdges();

  Util::cross(this->v12, this->v13, normal);
  float norm = Util::norm(normal);

  for(int i = 0; i < 3; ++i) {
    normal[i] /= norm;
  }
}

void Triangle::getNormal(float* p, float* normal) {
  for(int i = 0; i < 3; ++i) {
    normal[i] = this->normal[i];
  }
}

Collision* Triangle::collideWith(Ray& ray) {
  Collision* collision = nullptr;

#ifdef FAST_TRIANGLE
  bool intersect = false;
  float p[3] = {};

  // Fast, Minimum storage ray/triangle intersection
  // T.Moller, B.Trumbore

  float cross[3] = {};
  Util::cross(ray.getA(), v13, cross);
  float det = Util::dot(cross, v12);

  float dist[3] = {};
  for(int i = 0; i < 3; ++i) {
    dist[i] = ray.getB()[i] - p1[i];
  }
    
  float u = Util::dot(dist, cross) / det;
    
  Util::cross(dist, v12, cross);
  float v = Util::dot(ray.getA(), cross) / det;
  float t = 1 - u - v;

  for(int i = 0; i < 3; ++i) {
    p[i] = t*p1[i] + u*p2[i] + v*p3[i];
  }

  intersect = !( u < 0 || v < 0 || (u+v) > 1 );

  if (intersect) {
    for(int i = 0; i < 3; ++i) {
      p[i] = t*p1[i] + u*p2[i] + v*p3[i];
    }
    collision = new Collision(p, this);
  }

  // END TRIANGLE TEST
#else

  // Ray-triangle intersection in two steps :
  //   1) Plane projection
  //   2) Check if the projection is inside the triangle
  //      (half-space equivalent of the 2D half-plane case)

  Plane p;
  p.setOrigin(p1);
  p.setNormal(normal);
  Collision* withPlane = p.collideWith(ray);

  if (withPlane) {
    collision = new Collision(withPlane->position, this);
    delete withPlane;
    withPlane == nullptr;

    float cross[3] = {};

    Util::cross(v12, normal, cross);
    p.setOrigin(p1);
    p.setNormal(cross);
    bool s1 = p.whichSide(collision->position);
    
    Util::cross(v23, normal, cross);
    p.setOrigin(p2);
    p.setNormal(cross);
    bool s2 = p.whichSide(collision->position);

    float v31[3] = {-v13[0], -v13[1], -v13[2]};
    Util::cross(v31, normal, cross);
    p.setOrigin(p3);
    p.setNormal(cross);
    bool s3 = p.whichSide(collision->position);

    if (s1 != s2 || s2 != s3) {
      delete collision;
      collision = nullptr;
    }

  }

#endif

  return collision;
}

Mesh::Mesh() : Object() {}

Mesh::~Mesh() {
    for(Triangle* tr : triangles) {
      delete tr;
    }
}

int Mesh::addVertex(float x, float y, float z) {
  float* vertex = new float[3];
  vertex[0] = x;
  vertex[1] = y;
  vertex[2] = z;
  vertices.push_back(vertex);

  return vertices.size() - 1;
}

int Mesh::addTriangle(int v1, int v2, int v3) {
  int count = vertices.size();
  
  if (v1 >= count || v2 >= count || v3 >= count) {
    std::cerr << "Error: vertex index isout of bounds" << std::endl;
    return -1;
  }

  Triangle* t = new Triangle(this->material);
  t->setPoints(vertices[v1], vertices[v2], vertices[v3]);
  triangles.push_back(t);

  return triangles.size() - 1;
}


Collision* Mesh::collideWith(Ray& ray){
  Collision* nearestCollision = nullptr;
  float nearestDist = std::numeric_limits<float>::max();

  for(Triangle* tr : triangles) {
    Triangle& t = *tr;
    Collision* lastCollision = t.collideWith(ray);
    
    if (lastCollision) {
      float dist = Util::distance(ray.getB(), lastCollision->position);
      
      if (dist < nearestDist) {
	if (!nearestCollision) {
	  free(nearestCollision);
	}
	nearestCollision = lastCollision;
	nearestDist = dist;
      }
      else {
	delete lastCollision;
      }
    }
  }

  return nearestCollision;
}

void Mesh::getNormal(float* p, float* normal) {
  // The collideWith method sets the normal itself
  // in the returned collideWith method. Cast a ray
  // to your collision point if you really need it.
}


Mesh* Mesh::readfromObj(std::string fileName) {
  Mesh* m = new Mesh();

  std::ifstream file;
  std::string line;
  char c;
  float f1, f2, f3;
  int i1, i2, i3, i4;

  int nV=0, nM=0, nT=0;
  int indexShift = 1;

  file.open(fileName + ".obj");
  
  if (!file.is_open()) {
   std::cerr << "Could not find model " << fileName << std::endl;
   exit(1);
  }

  while (!file.eof()) {
    std::getline(file, line);

     std::stringstream sStream(line);

    if (line[0] == 'c') {
      // This marks a custom model, so do not shift the indices
      indexShift = 0;
    }

    if (line[0] == 'v') {
      sStream >> c >> f1 >> f2 >> f3;
      m->addVertex(f1, f2, f3);
      ++nV;
    }

    if (line[0] == 't' && line[1] == 'c') {
      sStream >> c >> c >> i1 >> i2 >> i3;
      Material* mat = new Material(i1, i2, i3);
      m->materials.push_back(mat);
      ++nM;
    }

    if (line[0] == 'f') {
      i4 = -1;
      sStream >> c >> i1 >> i2 >> i3 >> i4;
      int i = m->addTriangle(i1-indexShift, i2-indexShift, i3-indexShift);
      if (i4 > 0) {
	m->triangles[i]->swapMaterial(m->materials[i4-1]);
      }
      ++nT;
    }

  }

  file.close();
  std::cout << "Vertices:  " << nV << std::endl
	    << "Triangles: " << nT << std::endl
	    << "Materials: " << nM << std::endl;
  return m;
}

void Mesh::setPosition(float x, float y, float z) {
  float diff[] = {x - this->position[0], y - this->position[1], z - this->position[2]};

  for (float* vertex : vertices) {
    for(int i = 0; i < 3; ++i) {
      vertex[i] += diff[i];
    }
  }

  for (Triangle* t : triangles) {
    t->setPoints(t->p1, t->p2, t->p3);
  }

}


void Mesh::getPosFrom(float* pos, float* newPos)
{
}

void Triangle::getPosFrom(float* pos, float* newPos)
{
}