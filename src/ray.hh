#ifndef __RAY__HH__
#define __RAY__HH__

#include <cstdint>
#include <iostream>

#include "util.hh"

/**
 * A ray is a straight line, which can be described
 * by the equation p = a*t + b, with 'a' a 3D vector
 * and b a point belonging to the straight line
 */
class Ray{
private:

  float a[3];
  float b[3];

public:

  Ray();
  ~Ray();

  Ray& from(float x, float y, float z);
  Ray& to(float x, float y, float z);
  Ray& from(float* p);
  Ray& to(float* p);

  float* getA();
  float* getB();

};

std::ostream& operator<<(std::ostream& o, Ray& ray);

#endif
