
all: compile
	./rayetcarr

compile: clean
	clear
	g++ src/*.cc -o rayetcarr -std=c++0x -pthread -g -ggdb3

clean:
	rm -rf *~ src/*~
	rm -rf test*.bmp
	rm -rf rayetcarr
